"use strict";

const defaultArtist = "Hits & Mix";
const defaultTitle = "Station Millenium";
const defaultImage = "/logo-titrage.jpeg";
const overlayClassName = "hidden";

function setDefaults(overlayElement, artistElement, titleElement, imageElement) {
    if (artistElement.textContent !== defaultArtist && titleElement.textContent !== defaultTitle) {
        overlayElement.classList.add(overlayClassName);
        setTimeout(() => {
            artistElement.textContent = defaultArtist;
            titleElement.textContent = defaultTitle;
            imageElement.setAttribute("src", defaultImage);
            overlayElement.classList.remove(overlayClassName);
        }, 400)
    }
}

function getPLayerTitle(overlayElement, artistElement, titleElement, imageElement) {
    new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.open('GET', "/coverart/api/v1/getLastTrack");
        req.onload = () => req.status === 200 ? resolve(req.responseText) : reject(Error(req.statusText));
        req.onerror = (e) => reject(Error(e.toString()));
        req.send();
    }).then((dataString) => {
        const data = JSON.parse(dataString);
        if (data.is_track) {
            if (artistElement.textContent !== data.artist && titleElement.textContent !== data.title) {
                overlayElement.classList.add(overlayClassName);
                setTimeout(() => {
                    artistElement.textContent = `${data.artist}`;
                    titleElement.textContent = `${data.title}`;
                    if (data.is_image) {
                        imageElement.setAttribute("src", `/coverart/api/v1/getImage/${data.time}`);
                    } else {
                        imageElement.setAttribute("src", "/logo-titrage.jpeg");
                    }
                    overlayElement.classList.remove(overlayClassName);
                }, 400)
            }
        } else {
            setDefaults(overlayElement, artistElement, titleElement, imageElement);
        }
    }, (e) => {
        console.error(`Error: ${e}`);
        setDefaults(overlayElement, artistElement, titleElement, imageElement);
    })
}

window.onload = () => {
    const overlayElement = document.getElementById("overlay");
    const artistElement = document.getElementById("artist");
    const titleElement = document.getElementById("title");
    const imageElement = document.getElementById("image");

    getPLayerTitle(overlayElement, artistElement, titleElement, imageElement);
    setInterval(() => {
            getPLayerTitle(overlayElement, artistElement, titleElement, imageElement);
        }, 5000
    );
};
